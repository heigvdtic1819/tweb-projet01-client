import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  // serverAddr: string = 'localhost:3000';
  serverAddr: string = 'https://tweblabo01.herokuapp.com';

  constructor(private httpClient: HttpClient) { }

  loadComparison(comparisonId: string): Promise<any> {
    return this.httpClient
      .get(`${this.serverAddr}/comparison/${comparisonId}`)
      .toPromise();
  }

  loadComparisons(): Promise<any> {
    return this.httpClient
      .get(`${this.serverAddr}/latests`)
      .toPromise();
  }

  loadRepo(userName: string, repoName: string): Promise<any> {
    return this.httpClient
      .get(`${this.serverAddr}/languages/repo/${userName}/${repoName}`)
      .toPromise();
  }

  loadContrib(comparisonId: string, userName: string): Promise<any> {
    return this.httpClient
      .get(`${this.serverAddr}/languages/user/${comparisonId}/${userName}`)
      .toPromise();
  }

  loadInfosContrib(userName: string): Promise<any> {
    return this.httpClient
      .get(`${this.serverAddr}/user/${userName}`)
      .toPromise();
  }

  unloadContrib(comparisonId: string, userId: string): Promise<any> {
    return this.httpClient
      .delete(`${this.serverAddr}/comparison/${comparisonId}/user/${userId}`)
      .toPromise();
  }
}
