import { ApplicationRef, Component } from '@angular/core';
import { ScrollToConfigOptions, ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  // View states variables
  contribError : boolean = false;
  repoError : boolean = false;
  userLoading : boolean = false;
  repoLoading : boolean = false;
  statsShowing : boolean = false;
  isDatasFiltered: boolean = false;
  isDatasLoaded: boolean = false;
  step: number = 0;

  // App global
  title : string = 'Github analytics';
  userName : string = '';
  repoName : string = '';
  repoStats : any[];
  comparisonId : string = '';
  contribName : string = '';
  contributors: Map<string, any>;
  podiumContributors: any[];
  contributorsName: any[];
  latestsComparisons: any[];
  barDatas: any = {};
  barChart: any;
  pieCharts: any[];
  pieDatas: any[];
  colors: string[] = [
    'rgb(188, 170, 164)',
    'rgb(255, 245, 157)',
    'rgb(128, 203, 196)',
    'rgb(144, 202, 249)',
    'rgb(206, 147, 216)'];

  constructor(private scrollToService: ScrollToService,
              private httpClient: HttpClient,
              private activatedRoute: ActivatedRoute,
              private callService: ApiService,
              private appRef: ApplicationRef) {
  }

  ngOnInit() {
    this.initStructures();
    this.activatedRoute.queryParams
      .subscribe((params) => {
        // If Want to see an existing comparison
        if (params.comparisonId !== undefined) {
          this.loadExistingComparison(params.comparisonId);
        } else {// Else load the app normally
          this.callService.loadComparisons()
            .then(comparisons => this.latestsComparisons = comparisons);
        }
      });

    // Manage bootstrap tab
    $('#chartTab a').on('click', function (e) {
      e.preventDefault();
      $(this).tab('show');
    });
  }

  initStructures() {
    this.contributors = new Map<string, any>();
    this.contributorsName = [];
    this.podiumContributors = [];
    this.repoStats = [];
    this.pieCharts = [];
    this.pieDatas = [];
  }

  // Call if open with link or load with dropdown
  loadExistingComparison(comparisonId: string) {
    this.initStructures();
    this.repoLoading = true;
    // Load the comparison
    this.callService.loadComparison(comparisonId)
      .then((comparison) => {
        // Add each user to the main array comparison
        comparison.users.forEach((user) => {
          this.contribLoadEnd(user.id, user.name, user.stats);
        });

        // Update view by tell loading end
        this.comparisonLoadingEnd(comparison.id, comparison.stats);

        // Update the charts after adding all users
        this.updateChart();
        this.calculateScore();
      });
  }

  // Handle past event to autofill the rep name
  handlePast(event) {
    let content = event.clipboardData.getData('text/plain');
    if (content.indexOf('/') === -1) {
      return;
    }
    setTimeout(() => {
      content = content.split('/');
      this.userName = content[0];
      this.repoName = content[1];
    },         0);
  }

  newCompare(event) {
    // Stop the event propagation
    event.preventDefault();
    if (this.userName.length > 0 && this.repoName.length > 0) {
      this.repoLoading = true;
      this.callService.loadRepo(this.userName, this.repoName)
        .then((response) => {
          this.comparisonLoadingEnd(response.id, response.stats);
        })
        .catch((error) => {
          this.repoError = true;
          this.repoLoading = false;
        });
    }else {
      this.repoLoading = false;
      this.repoError = true;
    }
  }

  comparisonLoadingEnd(comparisonId: string, stats: any[]) {
    this.repoError = false;
    this.repoLoading = false;
    // Save the comparisonId
    this.comparisonId = comparisonId;
    this.repoStats =
      Object.entries(stats).map(([key, value]) => ({ key, value }));
    console.log(this.repoStats);
    // Refresh the view before construct charts
    this.appRef.tick();
    this.constructCharts();
    this.step += 1;
    this.scrollDown();
  }

  scrollDown() {
    this.statsShowing = true;

    // Let the event place at the end of the stack to be sure
    // the DOM has finished render the hidden part
    setTimeout(() => {
      const config: ScrollToConfigOptions = {
        target: 'bottom-part',
      };

      this.scrollToService.scrollTo(config);
    },         0);
  }

  addContrib(event) {
    event.preventDefault();
    if (this.comparisonId.length > 0 && this.contribName.length > 0) {
      this.userLoading = true;
      this.callService.loadContrib(this.comparisonId, this.contribName)
        .then((response) => {
          this.contribLoadEnd(response.id, this.contribName, response.stats);
          this.updateChart();
          this.calculateScore();
        })
        .catch((error) => {
          this.contribError = true;
          this.userLoading = false;
        });
    }
  }

  contribLoadEnd(userId: string, userName: string, userStats: any[]) {
    this.userLoading = false;
    const formattedResponse =
      Object.entries(userStats).map(([key, value]) => ({ key, value }));
    this.contributors.set(userId, { name: userName, stats: formattedResponse });
    this.contributorsName.push({
      id: userId,
      name: userName,
    });
    this.contribName = '';
  }

  calculateScore() {
    const podium: string[] = [];

    let totalLinesRepo = 0;
    this.repoStats.forEach(stat => totalLinesRepo += stat.value);

    const statProportion = {};
    this.repoStats.forEach(stat => statProportion[stat.key] = stat.value / totalLinesRepo * 100);

    if (this.contributors.size > 0) {
      const scores = [];
      let contributorScore = 0;
      this.contributors.forEach((contributor) => {
        contributor.stats.forEach((stat) => {
          if (statProportion[stat.key]) {
            contributorScore += stat.value * statProportion[stat.key] / 100;
          }
        });
        scores.push({ name: contributor.name, score: contributorScore });
      });

      scores.sort((a, b) => b.score - a.score);

      for (let i = 0; i < 3; i += 1) {
        if (scores[i]) {
          podium[i] = scores[i].name;
        }
      }
    }
    for (let i = 0; i < 3; i += 1) {
      this.callService.loadInfosContrib(podium[i])
        .then((contribInfos) => {
          this.podiumContributors[i] = contribInfos;
        });
    }
  }

  deleteContrib(contrib) {
    this.callService.unloadContrib(this.comparisonId, contrib.id)
      .then(() => {
        this.contributors.delete(contrib.id);
        for (let i = 0; i < this.contributorsName.length; i += 1) {
          if (this.contributorsName[i].id === contrib.id) {
            this.contributorsName.splice(i, 1);
            this.updateChart();
          }
        }
      });
  }

  updateChart() {
    this.isDatasFiltered = false;
    if (this.contributorsName.length > 0) {
      this.updateBarChart();
      this.updatePieChart();
      this.isDatasLoaded = true;
    }else {
      this.isDatasLoaded = false;
    }
  }

  updateBarChart() {
    const datasets = [];
    const labels = [];
    let colorIndex = 0;
    this.contributors.forEach((value, key, map) => {
      const datas = [];
      const stats = value.stats;
      stats.forEach((stat) => {
        const key = stat.key;
        const value = stat.value;
        let index = labels.indexOf(key);
        if (index === -1) {
          index = labels.push(key) - 1;
        }
        datas[index] = value;
      });

      datasets.push({
        label: value.name,
        backgroundColor: this.colors[colorIndex],
        data: datas,
      });
      colorIndex += 1;
    });
    this.barDatas.labels = labels;
    this.barDatas.datasets = datasets;

    // TODO calculate best min value to display
    this.filterDatasByValue(50000, -1, this.barDatas);
    this.barChart.update();
  }

  updatePieChart() {
    for (let i = 0; i < this.repoStats.length; i += 1) {
      const contributorValues = [];
      const languagesValues = [];
      this.contributors.forEach((value, key, map) => {
        console.log(value);
        const languageValue = value.stats[i].value;
        if (languageValue != null && languageValue !== 0) {
          contributorValues.push(value.name);
          languagesValues.push(languageValue);
        }
      });
      this.pieCharts[i].data.labels = contributorValues;
      this.pieCharts[i].data.datasets[0].data = languagesValues;
      this.pieCharts[i].data.datasets[0].backgroundColor = this.colors;
      this.pieCharts[i].update();
    }
  }

  getPieLine() {
    return [(Math.ceil(this.repoStats.length / 3) + 1)].fill(undefined).map((_, i) => i);
  }

  filterDatasByValue(minValue: number, maxValue: number, chartData) {
    for (let i = 0; i < chartData.labels.length; i += 1) {// Fir each languages
      let canDeletedLabel = true;
      // Search occurence of thise languages for each contributors(datasets)
      for (let j = 0; j < chartData.datasets.length; j += 1) {
        const contributor = chartData.datasets[j];
        const languageValue = contributor.data[i]; // languages at position i
        if (languageValue == null || languageValue < minValue) {
          continue;
        }
        canDeletedLabel = false;
      }
      if (canDeletedLabel) {
        chartData.datasets.forEach(item => item.data.splice(i, 1));
        chartData.labels.splice(i, 1);
        i -= 1;
      }
    }

    this.isDatasFiltered = true;
  }

  constructCharts() {
    if (this.barChart == null) {
      const ctx = document.getElementById('barChart');
      this.barChart = new Chart(ctx, {
        type: 'bar',
        data: this.barDatas,
        options: {
          barValueSpacing: 20,
          scales: {
            yAxes: [{
              ticks: {
                min: 0,
              },
            }],
          },
        },
      });
    }

    for (let i = 0; i < this.repoStats.length; i += 1) {
      if (this.pieCharts[i] == null) {
        this.pieDatas[i] = {datasets: [{
          data: [],
        }], labels: []};
        const canvas = document.getElementById(`pieChart${i}`);

        this.pieCharts[i] = new Chart(canvas, {
          type: 'pie',
          data: this.pieDatas[i],
          options: {
            legend: {
              display: false,
            },
            tooltips: {
              callbacks: {
                callbacks: {
                  label: tooltipItem => `${tooltipItem.yLabel}: ${tooltipItem.xLabel}`,
                  title: () => null,
                },
              },
            },
          },
        });
      }
    }
  }

  sharePage() {
    if (this.comparisonId.length > 0) {
      const url =
        `https://aebischerle.github.io/tweb_project1_client/?comparisonId=${this.comparisonId}`;
      const copyText = <HTMLInputElement>document.getElementById('input_copy_link');
      copyText.value = url;

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand('copy');

      const x = document.getElementById('snackbar');

      // Add the "show" class to DIV
      x.className = 'show';

      // After 3 seconds, remove the show class from DIV
      setTimeout(() => { x.className = x.className.replace('show', ''); }, 3000);
    }
  }

  openLink(link) {
    window.open(link);
  }
}
